# 书探

读书神器，Flutter框架开发，跨Android/iOS双平台，内置书源覆盖全网热门小说。

# 截图

![阅读](https://gitee.com/NightFarmer/seek_book/raw/master/screenshot/seekbook1.gif)

![搜索](https://gitee.com/NightFarmer/seek_book/raw/master/screenshot/seekbook2.png)

![搜索](https://gitee.com/NightFarmer/seek_book/raw/master/screenshot/seekbook3.png)

![详情](https://gitee.com/NightFarmer/seek_book/raw/master/screenshot/seekbook4.png)

![首页](https://gitee.com/NightFarmer/seek_book/raw/master/screenshot/seekbook5.png)

# 状态

基本功能完成，完善中。